import unittest
import argparse

from src.invitation_list_generator import generate_invitation_list_from_file
import tests


parser = argparse.ArgumentParser()
parser.add_argument('--run', nargs=1)
parser.add_argument('--test', action='store_true')

args = parser.parse_args()

if args.run:
    print(generate_invitation_list_from_file(input_file=args.run[0]))
elif args.test:
    suite = unittest.TestLoader().loadTestsFromModule(tests)
    unittest.TextTestRunner(verbosity=2).run(suite)
