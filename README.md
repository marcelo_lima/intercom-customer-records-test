# Customer Records
My implementation for the Customer Records test as part of the interview process for the Senior Software Engineer position at Intercom.

The worst-case complexity happens when sorting the output list, which in python (using TimSort in the *sorted* method) is `O(nlogn)`.

## Installation
Just clone the repository and make sure you have python 3+ installed on your machine.

## Running the code
From the project root folder, execute the following command:
```
$ python manage.py --run <INPUT_FILE_PATH>
```
And it will output the list of nearby people to invite.

## Running the tests
From the project root folder, execute the following command:
```
$ python manage.py --test
```