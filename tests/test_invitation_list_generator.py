import unittest

from src.invitation_list_generator import (
    find_nearby_people,
    sort_person_list_by_user_id,
    convert_person_list_to_display_format,
    generate_invitation_list_from_file,
)
from src.constants import (
    MAX_DISTANCE_KM,
    DESTINATION_COORDINATES_RAD,
)
from src.person import Person


class TestFindNearbyPeople(unittest.TestCase):
    def test_people_outside_max_distance_is_not_nearby(self):
        person_farway = Person(
            user_id=1,
            name='Test',
            # Rio de Janeiro coordinates
            latitude=-22.970722,
            longitude=-43.182365,
        )
        person_list = [person_farway]

        nearby_people = find_nearby_people(
            person_list,
            DESTINATION_COORDINATES_RAD,
            MAX_DISTANCE_KM,
        )

        assert person_farway not in nearby_people

    def test_people_within_max_distance_is_nearby(self):
        person_nearby = Person(
            user_id=1,
            name='Test',
            # Center of Dublin coordinates
            latitude=53.350140,
            longitude=-6.266155,
        )
        person_list = [person_nearby]

        nearby_people = find_nearby_people(
            person_list,
            DESTINATION_COORDINATES_RAD,
            MAX_DISTANCE_KM,
        )

        assert person_nearby in nearby_people

    def test_empty_person_list_returns_empty_list(self):
        person_list = []

        nearby_people = find_nearby_people(
            person_list,
            DESTINATION_COORDINATES_RAD,
            MAX_DISTANCE_KM,
        )

        assert len(list(nearby_people)) == 0


class TestSortPersonListByUserId(unittest.TestCase):

    def test_unsorted_list_is_sorted_by_user_id_asc(self):  # noqa
        person_nearby_id_1 = Person(
            user_id=1,
            name='Test 1',
            latitude=53.350140,
            longitude=-6.266155,
        )
        person_nearby_id_2 = Person(
            user_id=2,
            name='Test 2',
            latitude=53.350140,
            longitude=-6.266155,
        )

        unordered_person_list = [
            person_nearby_id_2,
            person_nearby_id_1,
        ]
        expected_result = [
            person_nearby_id_1,
            person_nearby_id_2,
        ]

        sorted_person_list = sort_person_list_by_user_id(
            unordered_person_list,
        )

        assert sorted_person_list == expected_result


class TestConvertPersonListToDisplayFormat(unittest.TestCase):
    def test_person_list_is_converted_to_list_of_user_id_and_name_tuples(self):  # noqa
        person_nearby = Person(
            user_id=1,
            name='Test 1',
            latitude=53.350140,
            longitude=-6.266155,
        )

        person_list = [person_nearby]

        expected_result = [person_nearby.to_display_format()]
        invitation_list = convert_person_list_to_display_format(
            person_list
        )

        assert invitation_list == expected_result


class TestGenerateInvitationListFromFile(unittest.TestCase):
    def test_invitation_list_is_generated_from_file(self):
        from unittest.mock import patch, mock_open
        mocked_file_lines = [
            '{"latitude": "53.350140", "user_id": 3, "name": "Test 3", "longitude": "-6.266155"}',  # noqa
            '{"latitude": "53.350140", "user_id": 2, "name": "Test 2", "longitude": "-6.266155"}',  # noqa
            '{"latitude": "-22.970722", "user_id": 1, "name": "Test 1", "longitude": "-43.182365"}',  # noqa
        ]
        mocked_open = mock_open()
        mocked_open.return_value.__iter__ = (
            lambda _: iter(mocked_file_lines)
        )
        expected_output = [(2, 'Test 2'), (3, 'Test 3')]

        with patch("builtins.open", mocked_open):
            invitation_list = generate_invitation_list_from_file('any/path/')

        assert invitation_list == expected_output
