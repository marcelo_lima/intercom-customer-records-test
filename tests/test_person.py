import unittest

from src.person import Person
from src.formulas import great_circle_distance, RadianCoordinates
from src.constants import DESTINATION_COORDINATES_RAD


class TestPersonMethods(unittest.TestCase):
    def test_to_display_format_returns_user_id_and_name_tuple(self):
        person = Person(
            user_id=1,
            name='Test',
            latitude=53.339428,
            longitude=-6.257664,
        )
        expected_display_format = (1, 'Test')

        assert person.to_display_format() == expected_display_format

    def test_distance_to_uses_great_circle_distance(self):
        person = Person(
            user_id=1,
            name='Test',
            latitude=0,
            longitude=0,
        )

        expected_result = great_circle_distance(
            person.coordinates_rad,
            DESTINATION_COORDINATES_RAD,
        )
        result = person.distance_to(
            DESTINATION_COORDINATES_RAD
        )

        assert result == expected_result

    def test_person_constructor_converts_degree_to_rads(self):
        person = Person(
            user_id=1,
            name='Test',
            latitude=53.339428,
            longitude=-6.257664,
        )
        expected_result = RadianCoordinates(
            0.9309486397304539,
            -0.10921684028351844
        )

        assert person.coordinates_rad == expected_result
