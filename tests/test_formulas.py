import unittest
from math import radians
from haversine import haversine

from src.formulas import (
    RadianCoordinates,
    great_circle_distance,
)


class TestFormulas(unittest.TestCase):
    def test_great_circle_distance_between_points_more_than_100km_away(self):
        # Dublin, IE
        point1 = RadianCoordinates(radians(53.3331), radians(-6.2489))
        # Limerick, IE
        point2 = RadianCoordinates(radians(52.668018), radians(-8.630498))

        expected_distance = 175.738
        distance = great_circle_distance(point1, point2)

        assert round(distance, 3) == expected_distance

    def test_great_circle_distance_between_points_less_than_100km_away(self):
        # Dublin, IE
        point1 = RadianCoordinates(radians(53.3331), radians(-6.2489))
        # Bray, IE
        point2 = RadianCoordinates(radians(53.2028), radians(-6.0983))

        expected_distance = 17.619
        distance = great_circle_distance(point1, point2)

        assert round(distance, 3) == expected_distance
