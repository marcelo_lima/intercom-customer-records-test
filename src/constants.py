from src.formulas import RadianCoordinates


EARTH_RADIUS_KM = 6373
DESTINATION_COORDINATES_RAD = RadianCoordinates(
    0.9309486397304539,
    -0.10921684028351844
)
MAX_DISTANCE_KM = 100
