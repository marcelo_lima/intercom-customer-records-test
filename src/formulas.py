from collections import namedtuple
from math import sin, sqrt, atan2, cos


RadianCoordinates = namedtuple('RadianCoordinates', 'lat lng')


def great_circle_distance(point1, point2):
    from src.constants import EARTH_RADIUS_KM

    distance_lat = abs(point2.lat - point1.lat)
    distance_lng = abs(point2.lng - point1.lng)

    val = (
        sin(distance_lat / 2) ** 2 +
        sin(distance_lng / 2) ** 2 * cos(point1.lat) * cos(point2.lat)
    )

    ang = 2 * atan2(sqrt(val), sqrt(1-val))

    return EARTH_RADIUS_KM * ang
