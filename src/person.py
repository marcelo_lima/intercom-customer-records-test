from math import radians

from src.formulas import great_circle_distance, RadianCoordinates


class Person:
    def __init__(self, name, user_id, latitude, longitude):
        self.name = name
        self.user_id = user_id

        latitude_rad = radians(float(latitude))
        longitude_rad = radians(float(longitude))

        self.coordinates_rad = RadianCoordinates(latitude_rad, longitude_rad)

    def distance_to(self, to_coordinates_rad):
        distance = great_circle_distance(
            self.coordinates_rad,
            to_coordinates_rad
        )

        return distance

    def to_display_format(self):
        return (self.user_id, self.name)
