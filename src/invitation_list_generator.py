import json

from src.constants import (
    DESTINATION_COORDINATES_RAD,
    MAX_DISTANCE_KM,
)
from src.person import Person


def find_nearby_people(person_list, destination_coord_rad, max_distance_km):
    nearby_people = (
        person for person in person_list
        if person.distance_to(destination_coord_rad) <= max_distance_km
    )

    return nearby_people


def sort_person_list_by_user_id(person_list):
    sorted_person_list = sorted(person_list, key=lambda person: person.user_id)

    return sorted_person_list


def convert_person_list_to_display_format(person_list):
    person_list_output = map(
        lambda person: person.to_display_format(),
        person_list,
    )

    return list(person_list_output)


def generate_invitation_list_from_file(input_file):
    with open(input_file) as f:
        # Wrap the file read generator in another generator that already
        # converts each json line into a Person object
        person_list = (Person(**json.loads(line)) for line in f)

        nearby_person_list = find_nearby_people(
            person_list,
            DESTINATION_COORDINATES_RAD,
            MAX_DISTANCE_KM,
        )

        sorted_person_list = sort_person_list_by_user_id(nearby_person_list)

    invitation_list = convert_person_list_to_display_format(
        sorted_person_list
    )

    return invitation_list
